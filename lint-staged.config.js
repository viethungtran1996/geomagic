// lint-staged.config.js

module.exports = {
    // Type check TypeScript files
    // '**/*.(ts|tsx)': (filenames) => `yarn tsc --noEmit ${filenames.join(' ')}`,

    // Format MarkDown and JSON
    '**/*.(md|json)': (filenames) =>
        `yarn prettier --write ${filenames.join(' ')}`,

    '**/*.{html,jsx,js,ts,tsx}': (filenames) => [
        `yarn eslint --fix ${filenames.join(' ')}  --ignore-pattern '!<relative/path/to/filename>'`,
        `yarn prettier --write ${filenames.join(' ')}`,
    ],

    '**/*.{css}': (filenames) => [
        `yarn stylelint --allow-empty-input ${filenames.join(' ')}`,
        `yarn prettier --write ${filenames.join(' ')}`,
    ],
}
