// A schema is a collection of type definitions (hence "typeDefs")
// that together define the "shape" of queries that are executed against
// your data.
const schema = `#graphql
    type User {
        name: String
        phone: String
    }

    type Query {
        users: [User]
    }
`

export default schema
