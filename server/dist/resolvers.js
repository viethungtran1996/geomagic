import users from './telefonbuch.json' assert { type: 'json' }
const resolvers = {
    Query: {
        users: () => users,
    },
}
export default resolvers
