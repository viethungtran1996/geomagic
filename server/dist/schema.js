const schema = `#graphql
    type User {
        name: String
        phone: String
    }

    type Query {
        users: [User]
    }
`
export default schema
