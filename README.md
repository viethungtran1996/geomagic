# README

## Requirements

-   yarn

## Getting Started

### 1. Clone Repository

Clone the existing repository to your local file directory using the following command:

```bash
git clone git@gitlab.com:viethungtran1996/geomagic.git
```

If you received these files as a package, simply unzip them.

### 2. Install Dependencies

Navigate into the file directory and install dependencies using the command:

```bash
yarn
```

This will fetch the required packages including lint-staged, linter, prettier, husky, and esLint.

### 3. Start Server

After the base setup, start the server with the command:

```bash
yarn server
```

The server will start running. It might take some time, but once it's ready, you can visit it at [localhost:4000](http://localhost:4000).

### 4. Start Client

Once the server is up and running, start the client in the same directory (`geomagic`) using the command:

```bash
yarn client
```

You can then visit the website at [localhost:3000](http://localhost:3000).

### Optional Steps

#### 1. Formatting Code

To format the code, use the following command:

```bash
yarn format
```

#### 2. Commit

After making changes, commit your changes using the command:

```bash
yarn commit
```

This is connected with Commitizen and ticket labeling for better organization.

#### 3. Pre-commit Checks

Before committing files to the working tree, you can pre-check them (esLint, prettier, and stylelint) with the command:

```bash
yarn pre-commit
```

#### 4. Testing Client

For testing the client, navigate into the directory of the client and use the following commands:

-   For unit tests:

```bash
yarn test
```

-   For end-to-end tests:

```bash
yarn cypress:open
```

That's it! You're now ready to use the Geomagic application with additional optional features. Enjoy!
