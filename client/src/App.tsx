import React from 'react'
import Layout from './Layout'
import { Paper } from '@mui/material'
import { useTheme } from '@mui/material/styles'
import { Link } from 'react-router-dom'

const App = () => {
    const theme = useTheme()

    return (
        <Layout>
            <Paper
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    gap: theme.spacing(2),
                    padding: {
                        xs: theme.spacing(1),
                        md: theme.spacing(2),
                    },
                    marginTop: theme.spacing(2),
                    boxSizing: 'border-box',
                    width: '100%',
                }}
            >
                <Link to={'/Telephonebook'}> Telephonebook 1 </Link>
                <Link to={'/Telephonebook2'}> Telephonebook 2 </Link>
            </Paper>
        </Layout>
    )
}

export default App
