// customFilter.test.js

import customFilter from '../../helpers/utils/filter'

describe('customFilter function', () => {
    it('filters an array of objects based on a key and value', () => {
        // Define an array of objects to test
        const elements = [
            { id: 1, name: 'Alice' },
            { id: 2, name: 'Bob' },
            { id: 3, name: 'Charlie' },
        ]

        // Test filtering by 'name' key and value 'Alice'
        const result = customFilter(elements, 'name', 'Alice')
        expect(result).toEqual([{ id: 1, name: 'Alice' }])

        // Test filtering by 'id' key and value  2
        const resultById = customFilter(elements, 'id', 2)
        expect(resultById).toEqual([{ id: 2, name: 'Bob' }])

        // Test filtering with a case-insensitive match
        const resultCaseInsensitive = customFilter(elements, 'name', 'alice')
        expect(resultCaseInsensitive).toEqual([{ id: 1, name: 'Alice' }])

        // Test filtering with a non-matching value
        const resultNoMatch = customFilter(elements, 'name', 'David')
        expect(resultNoMatch).toEqual([])
    })
})
