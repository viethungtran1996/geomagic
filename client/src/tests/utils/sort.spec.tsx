import customSort from '../../helpers/utils/sort'

describe('customSort function', () => {
    it('sorts an array of objects in ascending order by the specified key', () => {
        const elements = [
            { id: 3, name: 'Charlie' },
            { id: 1, name: 'Alice' },
            { id: 2, name: 'Bob' },
        ]

        const result = customSort(elements, 'id', 'ASC')
        expect(result).toEqual([
            { id: 1, name: 'Alice' },
            { id: 2, name: 'Bob' },
            { id: 3, name: 'Charlie' },
        ])
    })

    it('sorts an array of objects in descending order by the specified key', () => {
        const elements = [
            { id: 3, name: 'Charlie' },
            { id: 1, name: 'Alice' },
            { id: 2, name: 'Bob' },
        ]

        const result = customSort(elements, 'id', 'DESC')
        expect(result).toEqual([
            { id: 3, name: 'Charlie' },
            { id: 2, name: 'Bob' },
            { id: 1, name: 'Alice' },
        ])
    })
})
