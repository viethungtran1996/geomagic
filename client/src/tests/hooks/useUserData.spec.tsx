import { renderHook, act } from '@testing-library/react'
import useUserData from '../../helpers/hooks/useUserData'

describe('useUserData', () => {
    test('should change items', () => {
        const elements = [{ name: 'Okay', phone: '123123' }]

        const results = [
            { name: 'Charlie', phone: '3' },
            { name: 'Alice', phone: '1' },
            { name: 'Bob', phone: '2' },
        ]

        const { result } = renderHook(() => useUserData(elements))

        act(() => {
            result.current.setUsers(results)
        })

        expect(result.current.users).toEqual(results)
    })

    test('should sort items', () => {
        const elements = [
            { name: 'Charlie', phone: '3' },
            { name: 'Alice', phone: '1' },
            { name: 'Bob', phone: '2' },
        ]
        const { result } = renderHook(() => useUserData(elements))

        act(() => {
            result.current.setOrder({ order: 'ASC', orderBy: 'name' })
        })

        expect(result.current.users).toEqual([
            { name: 'Alice', phone: '1' },
            { name: 'Bob', phone: '2' },
            { name: 'Charlie', phone: '3' },
        ])
    })

    test('should filter items', () => {
        const elements = [
            { name: 'Charlie', phone: '3' },
            { name: 'Alice', phone: '1' },
            { name: 'Bob', phone: '2' },
        ]
        const { result } = renderHook(() => useUserData(elements))

        act(() => {
            result.current.setFilter({ filter: 'ha', filterBy: 'name' })
        })

        expect(result.current.users).toEqual([{ name: 'Charlie', phone: '3' }])
    })
})
