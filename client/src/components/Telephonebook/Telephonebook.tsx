import { Paper } from '@mui/material'
import { DataGrid, GridToolbar, GridColDef } from '@mui/x-data-grid'
import { useTheme } from '@mui/material/styles'
import { useQuery } from '@apollo/client'
import { Loading } from '../Loading'
import { Error } from '../Error'
import GET_USERS from '../../graphql/users'
import Layout from '../../Layout'
import TelephonebookType from '../../types/components/Telephonebook'

const columns: GridColDef[] = [
    {
        field: 'name',
        headerName: 'Name',
        sortable: true,
        editable: false,
        width: 200,
        type: 'string',
    },
    {
        field: 'phone',
        headerName: 'Phone',
        sortable: true,
        editable: false,
        width: 200,
        type: 'string',
    },
]

const Telephonebook = ({ users }: TelephonebookType): JSX.Element => {
    const theme = useTheme()

    return (
        <Layout>
            <Paper
                sx={{
                    display: 'flex',
                    overflow: 'hidden',
                    padding: {
                        xs: theme.spacing(1),
                        md: theme.spacing(2),
                    },
                    marginTop: theme.spacing(2),
                    boxSizing: 'border-box',
                    width: '100%',
                }}
            >
                <DataGrid
                    rows={users}
                    columns={columns}
                    getRowId={(row) => row.phone}
                    slots={{
                        toolbar: GridToolbar,
                    }}
                    disableRowSelectionOnClick
                    disableDensitySelector
                />
            </Paper>
        </Layout>
    )
}

const Loader = (): JSX.Element => {
    const { loading, error, data } = useQuery(GET_USERS)

    if (loading) {
        return <Loading />
    }

    if (error) {
        return <Error />
    }

    return <>{data?.users && <Telephonebook users={data.users} />}</>
}

export default Loader
