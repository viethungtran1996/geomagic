import UserType from '../../types/users'
import {
    TextField,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
    Box,
    useTheme,
} from '@mui/material'
import useUserData from '../../helpers/hooks/useUserData'
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward'
import { useQuery } from '@apollo/client'
import { Loading } from '../Loading'
import { Error } from '../Error'
import GET_USERS from '../../graphql/users'
import Layout from '../../Layout'
import TelephonebookType from '../../types/components/Telephonebook'

const Telephonebook2 = ({
    users: usersData,
}: TelephonebookType): JSX.Element => {
    const { users, setOrder, setFilter, filter, order } = useUserData(usersData)
    const theme = useTheme()

    const handleClick = (orderAfter: keyof UserType) => {
        if (order?.orderBy === orderAfter && order?.order === 'ASC') {
            setOrder({ order: 'DESC', orderBy: orderAfter })
        } else if (order?.orderBy === orderAfter && order?.order === 'DESC') {
            setOrder({ order: 'ASC', orderBy: orderAfter })
        } else {
            setOrder({ order: 'ASC', orderBy: orderAfter })
        }
    }

    return (
        <Layout>
            <Paper
                sx={{
                    display: 'flex;',
                    flexDirection: 'column',
                    gap: theme.spacing(2),
                    marginTop: theme.spacing(2),
                    padding: {
                        xs: theme.spacing(1),
                        md: theme.spacing(2),
                    },
                    boxSizing: 'border-box',
                    width: '100%',
                }}
            >
                <Box>
                    <TextField
                        id="outlined-basic"
                        label="Name"
                        variant="outlined"
                        value={filter?.filter || ''}
                        aria-label="name-textfield"
                        onChange={(event) =>
                            setFilter({
                                filter: event.currentTarget.value,
                                filterBy: 'name',
                            })
                        }
                    />
                </Box>

                <TableContainer component={Paper}>
                    <Table aria-label="simple table">
                        <TableHead aria-label="table-head">
                            <TableRow>
                                {(
                                    ['name', 'phone'] as unknown as [
                                        keyof UserType,
                                    ]
                                ).map((name: keyof UserType) => (
                                    <TableCell
                                        sx={{
                                            cursor: 'pointer',
                                        }}
                                        onClick={() => handleClick(name)}
                                        key={name}
                                        aria-label="table-head-cell"
                                    >
                                        <Box
                                            sx={{
                                                display: 'flex',
                                                justifyContent: 'flex-start',
                                                alignItems: 'center',
                                            }}
                                        >
                                            {' '}
                                            {name}
                                            {order?.orderBy === name && (
                                                <ArrowUpwardIcon
                                                    sx={{
                                                        transform:
                                                            order?.order ===
                                                            'DESC'
                                                                ? 'rotate(0deg)'
                                                                : 'rotate(180deg)',
                                                    }}
                                                />
                                            )}
                                        </Box>
                                    </TableCell>
                                ))}
                            </TableRow>
                        </TableHead>
                        <TableBody aria-label="table-body">
                            {users.map((row) => (
                                <TableRow
                                    key={row.phone}
                                    aria-label="table-body-row"
                                >
                                    <TableCell aria-label="table-body-name">
                                        {row.name}
                                    </TableCell>
                                    <TableCell aria-label="table-body-phone">
                                        {row.phone}
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Paper>
        </Layout>
    )
}

const Loader = (): JSX.Element => {
    const { loading, error, data } = useQuery(GET_USERS)

    if (loading) {
        return <Loading />
    }

    if (error) {
        return <Error />
    }

    return <>{data?.users && <Telephonebook2 users={data.users} />}</>
}

export default Loader
