import { Box, Typography, useTheme } from '@mui/material'
import { ReactComponent as Logo } from '../../assets/geomagic.svg'
import LocalPhoneIcon from '@mui/icons-material/LocalPhone'
import { Link } from 'react-router-dom'

const Header = () => {
    const theme = useTheme()
    return (
        <header>
            <Box
                sx={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    background: theme.palette.primary.main,
                    padding: {
                        xs: theme.spacing(1),
                        sm: theme.spacing(1),
                        md: theme.spacing(2),
                    },
                    gap: theme.spacing(2),
                }}
            >
                <Link to={'/'}>
                    <Box
                        sx={{
                            display: 'flex',
                            width: {
                                xs: theme.spacing(8),
                                sm: theme.spacing(20),
                                md: theme.spacing(25),
                            },
                        }}
                    >
                        <Logo style={{ height: 'auto' }} />
                    </Box>
                </Link>
                <Box
                    sx={{
                        display: 'flex',
                        flexDirection: {
                            xs: 'row',
                            sm: 'column',
                        },
                        justifyContent: 'center',
                        gap: theme.spacing(1),
                    }}
                >
                    {[
                        {
                            name: 'Telephonebook 1 ',
                            link: '/Telephonebook',
                            color: theme.palette.common.white,
                        },
                        {
                            name: 'Telephonebook 2 ',
                            link: '/Telephonebook2',
                            color: theme.palette.info.main,
                        },
                    ].map((element) => (
                        <Link to={element.link}>
                            <Box
                                sx={{
                                    display: 'flex',
                                    flexDirection: 'row',
                                    justifyContent: 'center',
                                    gap: theme.spacing(1),
                                }}
                            >
                                <Typography
                                    variant="body2"
                                    component="p"
                                    sx={{
                                        display: {
                                            xs: 'none',
                                            sm: 'flex',
                                        },
                                        color: element.color,
                                    }}
                                >
                                    {element.name}
                                </Typography>
                                <LocalPhoneIcon
                                    sx={{
                                        color: element.color,
                                    }}
                                />
                            </Box>
                        </Link>
                    ))}
                </Box>
            </Box>
        </header>
    )
}

export default Header
