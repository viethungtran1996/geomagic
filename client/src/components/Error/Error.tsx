import { Box, Typography } from '@mui/material'
import Layout from '../../Layout'
import { useTheme } from '@mui/material/styles'

const Error = () => {
    const theme = useTheme()

    return (
        <Layout>
            <Box
                sx={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: {
                        xs: theme.spacing(2),
                    },
                    padding: {
                        xs: theme.spacing(1),
                        md: theme.spacing(2),
                    },
                }}
            >
                <Box
                    sx={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    <Typography variant="h2" component="h2">
                        Something went wrong while fetching the data
                    </Typography>
                </Box>
            </Box>
        </Layout>
    )
}

export default Error
