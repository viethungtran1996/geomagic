import { Box } from '@mui/material'
import Layout from '../../Layout'
import loading from '../../assets/loading.gif'
import { useTheme } from '@mui/material/styles'

const Loading = () => {
    const theme = useTheme()

    return (
        <Layout>
            <Box
                sx={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: {
                        xs: theme.spacing(2),
                    },
                    padding: {
                        xs: theme.spacing(1),
                        md: theme.spacing(2),
                    },
                }}
            >
                <Box
                    sx={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    <img
                        style={{ width: '100%' }}
                        src={loading}
                        alt="loading..."
                        loading="lazy"
                    />
                </Box>
            </Box>
        </Layout>
    )
}

export default Loading
