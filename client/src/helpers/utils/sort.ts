import { OrderType } from '../../types/Order'

const customSort = <T extends { [key: string]: any }>(
    elements: T[],
    orderBy: keyof T,
    order: OrderType
): T[] => {
    if (order === 'DESC') {
        return elements.slice().sort((p1: T, p2: T) => {
            return p1[orderBy] < p2[orderBy]
                ? 1
                : p1[orderBy] > p2[orderBy]
                  ? -1
                  : 0
        })
    }

    if (order === 'ASC') {
        return elements
            .slice()
            .sort((p1: T, p2: T) =>
                p1[orderBy] > p2[orderBy]
                    ? 1
                    : p1[orderBy] < p2[orderBy]
                      ? -1
                      : 0
            )
    }

    return elements
}

export default customSort
