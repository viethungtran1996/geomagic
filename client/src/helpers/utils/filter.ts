const customFilter = <T extends { [key: string]: any }>(
    elements: T[],
    filterBy: keyof T,
    filterElement: string | number
) =>
    elements.filter((element) =>
        `${element[filterBy]}`
            .toLowerCase()
            .includes(`${filterElement}`.toLowerCase())
    )

export default customFilter
