import { useEffect, useState } from 'react'
import UserType from '../../types/users'
import { isEmpty } from 'lodash'
import { OrderType } from '../../types/Order'
import customSort from '../../helpers/utils/sort'
import customFilter from '../../helpers/utils/filter'

const useUserData = (usersData: UserType[]) => {
    const [users, setUsers] = useState<UserType[]>(usersData)
    const [order, setOrder] = useState<{
        order: OrderType
        orderBy: keyof UserType
    }>()
    const [filter, setFilter] = useState<{
        filter: string
        filterBy: keyof UserType
    }>()

    useEffect(() => {
        if (order?.orderBy && order?.order) {
            setUsers(customSort(users, order.orderBy, order.order))
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [order, setOrder])

    useEffect(() => {
        if (filter && !isEmpty(filter?.filter)) {
            setUsers(customFilter(usersData, filter.filterBy, filter.filter))
        } else {
            setUsers(usersData)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [filter])

    return {
        order,
        setOrder,
        filter,
        setFilter,
        users,
        setUsers,
    }
}

export default useUserData
