import React from 'react'
import { Header } from './components/Header'
import { Box } from '@mui/material'

const Layout = ({ ...props }) => {
    return (
        <Box
            className="App"
            sx={{
                position: 'relative',
            }}
        >
            <Header />
            {props.children}
        </Box>
    )
}

export default Layout
