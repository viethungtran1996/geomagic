const { REACT_APP_BACKEND_BASEURL } = process.env

const systemConfig = {
    backendBaseURL: REACT_APP_BACKEND_BASEURL || 'http://localhost:4000/',
}

export default systemConfig
