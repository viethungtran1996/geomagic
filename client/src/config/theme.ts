import { createTheme } from '@mui/material/styles'

const theme = createTheme({
    palette: {
        primary: {
            main: '#15243C',
        },
    },
    spacing: 16,
})

export default theme
