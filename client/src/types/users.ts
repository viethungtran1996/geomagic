interface UserType {
  name: string
  phone: string
}

export default UserType
