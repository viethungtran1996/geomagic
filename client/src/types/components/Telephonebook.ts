import UserType from '../users'

interface TelephonebookType {
    users: UserType[]
}

export default TelephonebookType
