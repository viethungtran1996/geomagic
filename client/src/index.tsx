import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import App from './App'
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client'
import { ThemeProvider } from '@mui/material/styles'
import theme from './config/theme'
import systemConfig from './config/systemconfig'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import { Telephonebook, Telephonebook2 } from './components/Telephonebook'

const client = new ApolloClient({
    uri: systemConfig.backendBaseURL,
    cache: new InMemoryCache(),
})

const router = createBrowserRouter([
    {
        path: '/',
        element: <App />,
    },
    {
        path: '/Telephonebook',
        element: <Telephonebook />,
    },
    {
        path: '/Telephonebook2',
        element: <Telephonebook2 />,
    },
])

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement)

root.render(
    <React.StrictMode>
        <ThemeProvider theme={theme}>
            <ApolloProvider client={client}>
                <RouterProvider router={router} />
            </ApolloProvider>
        </ThemeProvider>
    </React.StrictMode>
)
