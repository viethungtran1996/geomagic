import { gql } from '@apollo/client'

const GET_USERS = gql`
  {
    users {
      name
      phone
    }
  }
`

export default GET_USERS
