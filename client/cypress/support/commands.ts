/// <reference types="cypress" />
// ***********************************************
// This example commands.ts shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
//
// declare global {
//   namespace Cypress {
//     interface Chainable {
//       login(email: string, password: string): Chainable<void>
//       drag(subject: string, options?: Partial<TypeOptions>): Chainable<Element>
//       dismiss(subject: string, options?: Partial<TypeOptions>): Chainable<Element>
//       visit(originalFn: CommandOriginalFn, url: string, options: Partial<VisitOptions>): Chainable<Element>
//     }
//   }
// }

declare namespace Cypress {
    interface Chainable<Subject = any> {
        filter_check(
            type: string,
            existElement: string,
            nonExistElement: string
        ): Chainable<void>
        column_check(type: string): Chainable<void>
    }
}

// -- This is a child command --
Cypress.Commands.add(
    'filter_check',
    (type: string, existElement: string, nonExistElement: string) => {
        cy.get('input[placeholder*="Filter value"]').type(type)
        const existRegex = new RegExp(existElement, 'g')
        const nonExistRegex = new RegExp(nonExistElement, 'g')
        cy.contains(existRegex).should('exist')
        cy.contains(nonExistRegex).should('not.exist')
    }
)

Cypress.Commands.add('column_check', (type: string) => {
    const elementRegex = new RegExp(type, 'g')

    cy.get('[aria-label="Select columns"]').click()
    cy.get('[placeholder="Column title"]').type(type)
    cy.get('input.PrivateSwitchBase-input.MuiSwitch-input').click()
    cy.get('[aria-label="Select columns"]').click()
    cy.contains(elementRegex).should('not.exist')
})
