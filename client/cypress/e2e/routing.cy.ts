describe('Router Test', () => {
    it('Homepage', () => {
        cy.visit('/')
        cy.url().should('eq', 'http://localhost:3000/')
    })

    it('Telephonebook1', () => {
        cy.visit('/Telephonebook')
        cy.url().should('eq', 'http://localhost:3000/Telephonebook')
    })

    it('Telephonebook2', () => {
        cy.visit('/Telephonebook2')
        cy.url().should('eq', 'http://localhost:3000/Telephonebook2')
    })
})
