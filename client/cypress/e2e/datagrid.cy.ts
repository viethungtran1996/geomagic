describe('Telephonebook', () => {
    beforeEach(() => {
        cy.visit('/Telephonebook')
    })

    it('Filter Name', () => {
        cy.get('[aria-label="Show filters"]').click()
        cy.filter_check('Evie', 'Evie-Grace Sharpe', 'Harper-Rose Lennon')
    })

    it('Filter Phone', () => {
        cy.get('[aria-label="Show filters"]').click()
        cy.get('.MuiFormControl-root.MuiDataGrid-filterFormColumnInput')
            .find('select')
            .select('Phone')
        cy.filter_check('0171', '0171/24984023', '0176/33581541')
    })

    it('Columns Name', () => {
        cy.column_check('Name')
    })

    it('Columns Phone', () => {
        cy.column_check('Phone')
    })

    it('Sort Name ASC', () => {
        cy.get('.MuiDataGrid-columnHeader')
            .first()
            .find('.MuiDataGrid-columnHeaderTitleContainerContent')
            .click()
        cy.get('.MuiDataGrid-row')
            .first()
            .contains(/Abbigail Robles/g)
    })

    it('Sort Name DESC', () => {
        cy.get('.MuiDataGrid-columnHeader')
            .first()
            .find('.MuiDataGrid-columnHeaderTitleContainerContent')
            .click()
        cy.get('.MuiDataGrid-columnHeader')
            .first()
            .find('.MuiDataGrid-columnHeaderTitleContainerContent')
            .click()
        cy.get('.MuiDataGrid-row')
            .first()
            .contains(/Vincent Philip/g)
    })
})
