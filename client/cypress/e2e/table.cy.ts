describe('Telephonebook2', () => {
    beforeEach(() => {
        cy.visit('/Telephonebook2')
    })

    it('Filter Name', () => {
        cy.get('[aria-label="name-textfield"]').find('input').type('Evie')
        cy.contains(/Evie-Grace Sharpe/g).should('exist')
        cy.contains(/Harper-Rose Lennon/g).should('not.exist')
    })

    it('Sort Name ASC', () => {
        cy.get('[aria-label="table-head"]')
            .find('[aria-label="table-head-cell"]')
            .first()
            .click()
        cy.get('[aria-label="table-body"]')
            .find('[aria-label="table-body-row"]')
            .first()
            .contains(/Abbigail Robles/g)
    })

    it('Sort Name ASC', () => {
        cy.get('[aria-label="table-head"]')
            .find('[aria-label="table-head-cell"]')
            .first()
            .click()
        cy.get('[aria-label="table-head"]')
            .find('[aria-label="table-head-cell"]')
            .first()
            .click()
        cy.get('[aria-label="table-body"]')
            .find('[aria-label="table-body-row"]')
            .first()
            .contains(/Vincent Philip/g)
    })
})
